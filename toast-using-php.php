<script>
    <?php
    $q = '';
    $op = '';
    if(isset($_GET['q'])){
        $q = $_GET['q'];
    }

    if(isset($_GET['op'])){
        $op = $_GET['op'];
    }

    if($q === 'success' && $op =='insert'):
    ?>
        let toastHTML = "<span>New Contact Created Successfully!</span>";
        let classList = "green darken-1";
    <?php
    endif;
    ?>
    M.toast({
        html: toastHTML,
        classes: classList
    });
</script>
