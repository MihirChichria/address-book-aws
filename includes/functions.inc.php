<?php
function db_connect()
{
    static $connection;

    if(!$connection){
        //try to connect as the databse connection is not established!
        $config = parse_ini_file('config.ini');

        $connection = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
    }
    //I assure you that $connection has a valid connection established
    if($connection === false){
        return mysqli_connect_error();
    }
    return $connection;
}

function db_query($query)
{
    $connection = db_connect();
    $result = mysqli_query($connection,$query);
    return $result;
}

function db_error()
{
    $connection = db_connect();
    return mysqli_error($connection);
}
function db_select($query)
{
    $result = db_query($query);
    if($result === false){
        return false;
    }
    $rows = array();
    while($row = mysqli_fetch_assoc($result))
    {
        $rows[] = $row;
    }
    return $rows;
}

function db_quote($value)
{
    $connection = db_connect();
    return mysqli_real_escape_string($connection,$value);
}

function add_single_quotes($variable){
    return "'$variable'";
}

function db_update($table_name, $table_fields, $table_values, $condition){
    $sql = "UPDATE $table_name SET ".setAttr($table_fields, $table_values) . " WHERE $condition";
    echo $sql;
    $result = db_query($sql);
    dd($result);
    //db_error();

    // $query = "UPDATE contacts SET first_name=$first_name, last_name=$last_name, email=$email, birthdate=$birthdate, telephone=$telephone, address=$address, image_name=$image_name WHERE id = $id";
}

function setAttr($table_fields, $table_values){
    $string = "";
    $length = count($table_fields);
    for($i=0; $i<$length; $i++){
        $string .= $table_fields[$i]." = ".add_single_quotes($table_values[$i]).", ";
    }
    $string = substr($string, 0, -2);
    return $string;
}

function db_insert($table_name, $table_fields, $table_values){
    $sql = "INSERT INTO $table_name (".seperate(", ", $table_fields, false). ") VALUES(".seperate(", ", $table_values, true) . ")";
    // var_dump($sql);
    // echo $table_name;
    return db_query($sql);
}


function seperate($seperator, $array, $quote_status){
    $length = count($array);
    $string = "";

    for($i=0; $i<$length; $i++){
        if($quote_status)
            $array[$i] = add_single_quotes($array[$i])."$seperator ";
        else    
        $array[$i] .= "$seperator ";
        // var_dump($array[$i]);
        // var_dump("<br>");
        $string .= $array[$i];
    }
    $string = substr($string, 0, -3);
    // echo $string;
    // var_dump($array);
    return $string;
}

function dd($variable)
{
    die(var_dump($variable));
}

function redirect($url){
    header("Location: $url");
}