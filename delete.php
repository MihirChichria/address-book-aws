<?php
require_once("includes/functions.inc.php");

if(isset($_POST['delete_contact_id'])){
    $delete_id = $_POST['delete_contact_id'];
    $path = "images/users/";
    $img_name = db_select("SELECT image_name FROM contacts WHERE id=$delete_id");
    unlink($path.$img_name[0]['image_name']);
    $query = "DELETE FROM contacts WHERE id=$delete_id";
    $result = db_query($query);
    if($result){
        redirect("index.php?q=success&op=delete");
    }else{
        redirect("index.php?q=error&op=delete");
    }
}